const statuses = [
    {
        "name": "Draft",
        "color": "#C05717"
    },
    {
        "name": "Needs prep",
        "color": "#FFC58B"
    },
    {
        "name": "Needs estimates",
        "color": "#CBF0F0"
    },
    {
        "name": "Planned",
        "color": "#E2E6EE"
    },
    {
        "name": "Needs input",
        "color": "#B3BCF5"
    },
    {
        "name": "Waiting",
        "color": "#47C1BF"
    },
    {
        "name": "In progress",
        "color": "#50B83C"
    },
    {
        "name": "Needs testing",
        "color": "#FAD200"
    },
    {
        "name": "Testing",
        "color": "#FFEA8A"
    },
    {
        "name": "Needs review",
        "color": "#5C6AC4"
    },
    {
        "name": "Needs attention",
        "color": "#DE1818"
    },
    {
        "name": "Completed",
        "color": "#145B39"
    }
]

export default statuses;