
import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
      selectedStatus: {}
    },
    mutations: {
      SET_SELECTED_STATUS(state, status) {
        state.selectedStatus = status;
      }
    },
    actions: {
      setSelectedStatus(context, status) {
        context.commit('SET_SELECTED_STATUS', status);
      }
    }
  })

  export default store;